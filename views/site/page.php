<?php
    use app\models\User;
use yii\helpers\Url;

?>

<table>
    <?php foreach ($page as $model): ?>
    <tr>
        <td>Name: <?=$model->username?></td>
        <td>Email: <?=$model->email?></td>
        <td>
            <?php
            if ($model->status) {
                echo '<a class="btn btn-primary" href="' . Url::to(['/site/status' , 'id' => $model->id]) . '">Deactivate</a>';
            } else {
                echo '<a class="btn btn-primary" href="' . Url::to(['/site/status' , 'id' => $model->id]) . '">Activate</a>';
            }?>

        </td>
    </tr>
    <?php endforeach; ?>
</table>

<!--<a class="btn btn-primary" href="--><?php //=Url::to(['/site/status', 'id' => $model->id])?><!--">-->
<!--    Deactivate-->
<!--</a>-->