<?php

use yii\db\Migration;

/**
 * Class m231009_084259_lol
 */
class m231009_084259_lol extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('worksheett_disinfections', [
            'id' => $this->primaryKey(),
            'object_id' => $this->integer(),
            'type' => $this->string(255)->notNull(),
            'organization_name'=>$this->char(255),
            'adress'=>$this->string(255),
            'boxs'=>$this ->text()->notNull(),
            'area_all'=>$this->double()->notNull(),
            'unit'=>$this->string(50)->notNull()->defaultValue('м2'),
            'method_id'=>$this->integer()->notNull(),
            'type_id'=>$this->integer()->notNull(),
            'medicine_name'=>$this->string(255)->notNull(),
            'dosage'=>$this->string(50)->notNull(),
            'time'=>$this->integer(),
            'date_event'=>$this->integer()->notNull(),
            'report_date'=>$this->integer()->notNull(),
            'create_user_id'=>$this->integer()->notNull(),
            'start_processing'=>$this->string(255),
            'end_processing'=>$this->string(255),
            'exposition_time'=>$this->time(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m231009_084259_lol cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m231009_084259_lol cannot be reverted.\n";

        return false;
    }
    */
}
