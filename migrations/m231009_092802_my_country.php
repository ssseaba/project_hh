<?php

use yii\db\Migration;

/**
 * Class m231009_092802_my_country
 */
class m231009_092802_my_country extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('country3', [
            'code' => $this->char(2)->notNull(),
            'name' => $this->char(52)->notNull(),
            'population' => $this->integer(11)->defaultValue(0),
        ]);
        $this->insert('country3', [
            'code' => 'AU',
            'name' => 'Australia',
            'population' => 24016400
        ]);
        $this->insert('country3', [
            'code' => 'BR',
            'name' => 'Brazil',
            'population' => 205722000
        ]);
        $this->insert('country3', [
            'code' => 'CA',
            'name' => 'Canada',
            'population' => 35985751
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m231009_092802_my_country cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m231009_092802_my_country cannot be reverted.\n";

        return false;
    }
    */
}
