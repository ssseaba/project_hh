<?php

use yii\db\Migration;

/**
 * Class m231010_115219_add_column
 */
class m231010_115219_add_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{user}}', 'status', $this->integer()->defaultValue(0)->after('email'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m231010_115219_add_column cannot be reverted.\n";

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m231010_115219_add_column cannot be reverted.\n";

        return false;
    }
    */
}
